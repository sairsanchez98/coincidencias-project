<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/v1/auth/login' , [App\Http\Controllers\Auth\AuthController::class , 'login']);
Route::post('/v1/auth/register' , [App\Http\Controllers\Auth\AuthController::class, 'register']);

Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('/v1/search' , [App\Http\Controllers\DiccionarioController::class, 'search']);
});