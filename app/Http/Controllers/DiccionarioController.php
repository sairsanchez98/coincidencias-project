<?php

namespace App\Http\Controllers;

use App\Models\diccionario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DiccionarioController extends Controller
{

    public function search(Request $request)
    {
        $rules = [
            "nombres_apellidos" => "required|string",
            "porcentaje" => "required|numeric|min:0|max:100"
        ];
        $messages = [
            "nombres_apellidos.required" => "El campo nombre y apellidos es requerido.",
            "nombres_apellidos.string" => "El campo nombre y apellidos debe contener solo texto.",
            "porcentaje.required" => "El campo porcentaje es requerido.",
            "porcentaje.min" => "El porcentaje minimo debe ser 0.",
            "porcentaje.max" => "El porcentaje máximo debe ser 100."
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }


        $search =  diccionario::all();

        $results = array();
        foreach ($search as $key => $value) {
            similar_text($value->nombre, $request->nombres_apellidos, $percent);
            $percent =  round($percent);
            if ($percent == $request->porcentaje) {
                array_push($results, [
                    $value,
                    "percent" => $percent,
                ]);
            }
        }

        $message = "";
        if (count($results)  == 0) {
            $message = "Exitoso sin resultados.";
        } else {
            $message = "Exitoso con resultados.";
        }

        $response = ["results" => $results, "message" => $message];
        return $response;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\diccionario  $diccionario
     * @return \Illuminate\Http\Response
     */
    public function show(diccionario $diccionario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\diccionario  $diccionario
     * @return \Illuminate\Http\Response
     */
    public function edit(diccionario $diccionario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\diccionario  $diccionario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, diccionario $diccionario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\diccionario  $diccionario
     * @return \Illuminate\Http\Response
     */
    public function destroy(diccionario $diccionario)
    {
        //
    }
}
