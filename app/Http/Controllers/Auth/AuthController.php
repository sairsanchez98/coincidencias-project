<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request) 
    {
        
        $fields = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        
        $user = User::where('email', $fields['email'])->first();
       

        // Check password
        if(!$user || !Hash::check($fields['password'], $user->password)) {
            $response = ['message' => 'Bad creds'];
            return response($response, 401);
        }

        $token = $user->createToken('myapptoken')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 201);
    }

    public function register (Request $request)
    {

    
        $validator = Validator::make($request->all(),[
            'name' =>'required|string',
            'email' => 'required|string|unique:users,email',
            'password' => 'required|string|confirmed',
        ]);

        if ($validator->fails()) {

            return response()->json($validator->errors(), 422);

        }


        
        $user= User::create(
            [
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>bcrypt($request->password)
            ]
        );

   


        $token = $user->createToken('myapptoken')->plainTextToken;
        
        $response  = [
            'user' =>$user,
            'token' => $token 
        ];

        return response ($response , 201);
    }
}
